part of connect;
/**
 * 默认中间件
 */
class Base{
    void exec(HttpRequest req, HttpResponse res){
        res.headers.add("app", "connect v0.0.1");
        res.statusCode = HttpStatus.OK;
    }
}
