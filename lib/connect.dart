library connect;

import "dart:io";

part "handler.dart";
part "middleware/base.dart";

class Connect {

    List _middleWare = [];

    Map<String, Map<String, Handler>> _routes = {
        "_get": new Map<String, Handler>(),
        "_post": new Map<String, Handler>(),
        "_put": new Map<String, Handler>(),
        "_delete": new Map<String, Handler>()
    };

    Handler get(String url, Handler handler) => _routes['_get'].putIfAbsent(url, () => handler);

    Handler post(String url, Handler handler) => _routes['_post'].putIfAbsent(url, () => handler);

    Handler put(String url, Handler handler) => _routes['_put'].putIfAbsent(url, () => handler);

    Handler delete(String url, Handler handler) => _routes['_delete'].putIfAbsent(url, () => handler);

    void use(middleWare){
        _middleWare.add(middleWare);
    }

    /**
     * 请求路由没有配置时的默认处理
     */
    void _not_found_handle(HttpRequest request) {
        request.response.statusCode = HttpStatus.NOT_FOUND;
        request.response.write(new StringBuffer("${request.uri.toString()} not found"));
        request.response.close();
    }

    /**
     * 请求入口
     * 入口拦截请求，判断请求有无对应路由，如果没有找到配置，交由_not_found_handle处理
     * 如果请求存在，则将原始HttpRequest对象HttpResponse对象进行封装
     */
    List _entry(HttpRequest request){
        if (_routes['_${request.method.toLowerCase()}'].containsKey(request.uri.toString()))
            //包装req和res
            return [request, request.response];
        else
            _not_found_handle(request);
    }

    /**
     * 请求调度
     */
    Function _requestHandle(_server) {
        _server.listen((HttpRequest request) {
            List r = _entry(request);
            if(r != null){
                _middleWare.forEach((v){
                    v.exec(r[0], r[1]);
                });
                String uri = r[0].uri.toString();
                String method = r[0].method.toLowerCase();

                _routes['_${method}'][uri].exec(r[0], r[1]);
            }
        }, onError: (error) {
            print(error);
        });
    }

    void listen(int port, Function callback) {
        HttpServer.bind(InternetAddress.LOOPBACK_IP_V4, port).then(_requestHandle);
        if (callback != null)callback();
    }

    Connect() {
        this._middleWare.add(new Base());
    }

}
