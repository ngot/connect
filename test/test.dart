import "package:connect/connect.dart";
import "dart:io";

getHandle(HttpRequest req, HttpResponse res) {
    print(" get 来了");
    res.write("get reply");
    res.close();
}

postHandle(HttpRequest req, HttpResponse res) {
    print(" post 来了");
    res.write("post reply");
    res.close();
}

putHandle(HttpRequest req, HttpResponse res) {
    print(" put 来了");
    res.write("put reply");
    res.close();
}

deleteHandle(HttpRequest req, HttpResponse res) {
    print(" delete 来了");
    res.write("delete reply");
    res.close();
}

main() {
    //创建服务器对象
    var app = new Connect();

    final int PORT = 8080;

    //路由配置
    app.get("/get", new Handler(getHandle));
    app.post("/post", new Handler(postHandle));
    app.put("/put", new Handler(putHandle));
    app.delete("/delete", new Handler(deleteHandle));

    //启动监听
    app.listen(PORT, () {
        print("Server running at PORT: $PORT");
    });

}
